﻿using AutoMapper;
using EpamShop.Domain.DTO;
using EpamShop.Domain.Orders;
using EpamShop.Repository;
using EpamShop.Repository.Interfaces;
using EpamShop.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.Services.Services
{
    public class OrderService : IOrderService
    {
        private IUnitOfWork UnitOfWork { get; set; }
        private IMapper Mapper { get; set; }
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }
        public async Task AddAsync(OrderDTO orderDTO)
        {
            var order = Mapper.Map<Order>(orderDTO);
            await UnitOfWork.OrderRepository.AddAsync(order);
            UnitOfWork.UserRepository.GetAll().First(u => u.Name == orderDTO.Owner).Orders.ToList().Add(order);
            await UnitOfWork.SaveAsync();
        }

        public IEnumerable<OrderDTO> GetAll()
        {
            return UnitOfWork.OrderRepository.GetAll().Select(p => Mapper.Map<OrderDTO>(p)).ToList();
        }

        public async Task<OrderDTO> GetByIdAsync(int id)
        {
            var order = await UnitOfWork.OrderRepository.GetItemByIdAsync(id);
            return Mapper.Map<OrderDTO>(order);
        }

        public async Task UpdateAsync(OrderDTO orderDTO)
        {
            UnitOfWork.OrderRepository.Update(Mapper.Map<Order>(orderDTO));
            await UnitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await UnitOfWork.OrderRepository.DeleteItemByIdAsync(id);
            await UnitOfWork.SaveAsync();
        }
    }
}
