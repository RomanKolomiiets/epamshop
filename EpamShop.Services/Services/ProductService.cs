﻿using AutoMapper;
using EpamShop.Domain;
using EpamShop.Domain.Products;
using EpamShop.Repository.Interfaces;
using EpamShop.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.Services.Services
{
    public class ProductService : IProductService
    {
        private IUnitOfWork UnitOfWork { get; set; }
        private IMapper Mapper { get; set; }
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            return UnitOfWork.ProductRepository.GetAll().Select(p => Mapper.Map<ProductDTO>(p)).ToList();
        }

        public async Task<ProductDTO> GetByIdAsync(int id)
        {
            var product = await UnitOfWork.ProductRepository.GetItemByIdAsync(id);
            return Mapper.Map<ProductDTO>(product);
        }

        public async Task AddAsync(ProductDTO productDTO)
        {
            var product = Mapper.Map<Product>(productDTO);
            await UnitOfWork.ProductRepository.AddAsync(product);
            UnitOfWork.UserRepository.GetAll().First(u => u.Name == productDTO.Owner).Products.ToList().Add(product);
            await UnitOfWork.SaveAsync();       
        }

        public async Task UpdateAsync(ProductDTO productDTO)
        {
            UnitOfWork.ProductRepository.Update(Mapper.Map<Product>(productDTO));
            await UnitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await UnitOfWork.ProductRepository.DeleteItemByIdAsync(id);
            await UnitOfWork.SaveAsync();
        }
    }
}
