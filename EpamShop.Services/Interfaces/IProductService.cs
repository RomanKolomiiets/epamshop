﻿using EpamShop.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EpamShop.Services.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductDTO> GetAll();
        Task<ProductDTO> GetByIdAsync(int id);
        Task AddAsync(ProductDTO productDTO);
        Task UpdateAsync(ProductDTO productDTO);
        Task DeleteByIdAsync(int id);
    }
}
