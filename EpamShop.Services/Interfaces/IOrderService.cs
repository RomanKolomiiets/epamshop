﻿
using EpamShop.Domain.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EpamShop.Services.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<OrderDTO> GetAll();
        Task<OrderDTO> GetByIdAsync(int id);
        Task AddAsync(OrderDTO orderDTO);
        Task UpdateAsync(OrderDTO orderDTO);
        Task DeleteByIdAsync(int id);
    }
}
