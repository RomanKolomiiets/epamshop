﻿using AutoMapper;
using EpamShop.Domain;
using EpamShop.Domain.DTO;
using EpamShop.Domain.Orders;
using EpamShop.Domain.Products;
using System.Linq;

namespace EpamShop.Services
{
    public class Automapper : Profile
    {
        public Automapper()
        {
            CreateMap<Product, ProductDTO>()
                .ForMember(p => p.Price, p => p.MapFrom(p => p.Price))
                .ForMember(c => c.Category, c => c.MapFrom(c => c.Category))
                .ForMember(o => o.Owner, o => o.MapFrom(o => o.Owner))
                .ForMember(c => c.Comment, c => c.MapFrom(c => c.Comment))
                .ForMember(p => p.ProductName, p => p.MapFrom(p => p.ProductName));
            
            CreateMap<Order, OrderDTO>()
                .ForMember(t => t.TotalPrice, t => t.MapFrom(t => t.TotalPrice))
                .ForMember(c => c.CreateTime, c => c.MapFrom(c => c.CreateTime))
                .ForMember(o => o.Owner, o => o.MapFrom(o => o.Owner))
                .ForMember(p => p.Products, p => p.MapFrom(p => p.Products));
        }
    }
}
