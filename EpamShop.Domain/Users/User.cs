﻿using EpamShop.Domain.Orders;
using EpamShop.Domain.Products;
using System.Collections.Generic;

namespace EpamShop.Domain.Users
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public IEnumerable<Product> Products { get; set; }

        public User(string name, string email, string phoneNumber)
        {
            Name = name;
            Email = email;
            PhoneNumber = phoneNumber;
        }
    }
}
