﻿using EpamShop.Domain.Products;
using System.Collections.Generic;

namespace EpamShop.Domain
{
    public class ProductDTO
    {
        public string ProductName { get; set; }
        public int Price { get; set; }

        public string Category { get; set; }
        public string Owner { get; set; }

        public IEnumerable<Comment> Comment { get; set; }
    }
}
