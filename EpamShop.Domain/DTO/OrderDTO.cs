﻿using System;
using System.Collections.Generic;

namespace EpamShop.Domain.DTO
{
    public class OrderDTO
    {
        public decimal TotalPrice { get; set; }

        public DateTime CreateTime { get; set; }

        public IEnumerable<string> Products { get; set; }

        public string Owner { get; set; }
    }
}
