﻿using EpamShop.Domain.Users;
using System;

namespace EpamShop.Domain.Products
{
    public class Comment : BaseEntity
    {
        public DateTime CreateTime { get; set; }

        public string Text { get; set; }

        public User User { get; set; }

        public Comment(DateTime createTime, string text, User user)
        {
            CreateTime = createTime;
            Text = text;
            User = user;
        }

    }
}
