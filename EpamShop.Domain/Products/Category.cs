﻿namespace EpamShop.Domain.Products
{
    public enum Category
    {
        PC,
        MobilePhones,
        Appliances,
        Other
    }
}
