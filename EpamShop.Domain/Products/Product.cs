﻿using EpamShop.Domain.Users;
using System.Collections.Generic;

namespace EpamShop.Domain.Products
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public int Price { get; set; }

        public Category Category { get; set; }
        public User Owner { get; set; }

        public IEnumerable<Comment> Comment { get; set; }

        public Product(string productName, int price, Category category, User owner, IEnumerable<Comment> comment)
        {
            ProductName = productName;
            Price = price;
            Category = category;
            Owner = owner;
            Comment = comment;
        }
    }
}
