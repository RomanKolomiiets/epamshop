﻿using EpamShop.Domain.Products;
using EpamShop.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamShop.Domain.Orders
{
    public class Order : BaseEntity
    {
        public decimal TotalPrice { get; set; }

        public DateTime CreateTime { get; set; }

        public IEnumerable<Product> Products { get; set; }

        public User Owner { get; set; }

        public Order(IEnumerable<Product> products)
        {
            Products = products;
            CreateTime = DateTime.Now;
            TotalPrice = CalculateTotalPrice();
        }

        private decimal CalculateTotalPrice()
        {
            var result = Products.Sum(p => p.Price);
            return result;
        }

    }
}
