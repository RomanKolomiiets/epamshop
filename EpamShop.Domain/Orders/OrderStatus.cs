﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamShop.Domain.Orders
{
    public enum OrderStatus
    {
        New,
        CancelledByUser,
        CancelledByAdmin,
        Get,
        Paid,
        Sent
    }
}
