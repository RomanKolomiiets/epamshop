﻿
using EpamShop.Domain.Orders;
using EpamShop.Domain.Products;
using EpamShop.Domain.Users;
using System.Data.Entity;

namespace EpamShop.Repository
{
    public class ShopContext : DbContext
    {
        public ShopContext() : base("Shop") 
        {
            Database.SetInitializer(new ShopContextInitializer());
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }

    }
}
