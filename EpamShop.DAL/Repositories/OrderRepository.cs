﻿using EpamShop.Domain.Orders;
using EpamShop.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.Repository.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        protected ShopContext Context { get; set; }
        public OrderRepository(ShopContext context)
        {
            Context = context;
        }

        public async Task AddAsync(Order item)
        {
            Context.Set<Order>().Add(item);
            await Context.SaveChangesAsync();
        }

        public IEnumerable<Order> GetAll()
        {
            return Context.Set<Order>().AsEnumerable();
        }

        public async Task<Order> GetItemByIdAsync(int id)
        {
            return await Context.Set<Order>().FindAsync(id);
        }

        public void Update(Order item)
        {
            var updated = Context.Set<Order>().First(p => p.Id == item.Id);

            updated.TotalPrice = item.TotalPrice;
            updated.CreateTime = item.CreateTime;
            updated.Products = item.Products;
            updated.Owner = item.Owner;

            Context.SaveChanges();
        }

        public void Delete(Order item)
        {
            Context.Set<Order>().Remove(item);
            Context.SaveChanges();
        }

        public async Task DeleteItemByIdAsync(int id)
        {
            Context.Set<Order>().Remove(await GetItemByIdAsync(id));
            await Context.SaveChangesAsync();
        }
    }
}
