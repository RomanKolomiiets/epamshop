﻿using EpamShop.Domain.Users;
using EpamShop.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.Repository.Repositories
{
    public class UserRepository : IRepository<User>
    {
        protected ShopContext Context { get; set; }
        public UserRepository(ShopContext context)
        {
            Context = context;
        }

        public async Task AddAsync(User item)
        {
            Context.Set<User>().Add(item);
            await Context.SaveChangesAsync();
        }

        public IEnumerable<User> GetAll()
        {
            return Context.Set<User>().AsEnumerable();
        }

        public async Task<User> GetItemByIdAsync(int id)
        {
            return await Context.Set<User>().FindAsync(id);
        }

        public void Update(User item)
        {
            var updated = Context.Set<User>().First(p => p.Id == item.Id);

            updated.Name = item.Name;
            updated.Email = item.Email;
            updated.PhoneNumber = item.PhoneNumber;
            updated.Orders = item.Orders;

            Context.SaveChanges();
        }

        public void Delete(User item)
        {
            Context.Set<User>().Remove(item);
            Context.SaveChanges();
        }

        public async Task DeleteItemByIdAsync(int id)
        {
            Context.Set<User>().Remove(await GetItemByIdAsync(id));
            await Context.SaveChangesAsync();
        }
    }
}
