﻿using EpamShop.Domain.Products;
using EpamShop.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.Repository.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        protected ShopContext Context { get; set; }
        public ProductRepository(ShopContext context)
        {
            Context = context;
        }
      
        public async Task AddAsync(Product item)
        {
            Context.Set<Product>().Add(item);
            await Context.SaveChangesAsync();
        }

        public IEnumerable<Product> GetAll()
        {
            return Context.Set<Product>().AsEnumerable();
        }

        public async Task<Product> GetItemByIdAsync(int id)
        {
            return await Context.Set<Product>().FindAsync(id);
        }

        public void Update(Product item)
        {
            var updated = Context.Set<Product>().First(p => p.Id == item.Id);

            updated.Price = item.Price;
            updated.Category = item.Category;
            updated.Owner = item.Owner;
            updated.Comment = item.Comment;

            Context.SaveChanges();
        }

        public void Delete(Product item)
        {
            Context.Set<Product>().Remove(item);
            Context.SaveChanges();
        }

        public async Task DeleteItemByIdAsync(int id)
        {
            Context.Set<Product>().Remove(await GetItemByIdAsync(id));
            await Context.SaveChangesAsync();
        }
    }
}
