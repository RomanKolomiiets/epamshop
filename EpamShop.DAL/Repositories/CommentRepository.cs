﻿using EpamShop.Domain.Products;
using EpamShop.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.Repository.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {
        protected ShopContext Context { get; set; }
        public CommentRepository(ShopContext context)
        {
            Context = context;
        }

        public async Task AddAsync(Comment item)
        {
            Context.Set<Comment>().Add(item);
            await Context.SaveChangesAsync();
        }

        public IEnumerable<Comment> GetAll()
        {
            return Context.Set<Comment>().AsEnumerable();
        }

        public async Task<Comment> GetItemByIdAsync(int id)
        {
            return await Context.Set<Comment>().FindAsync(id);
        }

        public void Update(Comment item)
        {
            var updated = Context.Set<Comment>().First(p => p.Id == item.Id);

            updated.CreateTime = item.CreateTime;
            updated.Text = item.Text;
            updated.User = item.User;

            Context.SaveChanges();
        }

        public void Delete(Comment item)
        {
            Context.Set<Comment>().Remove(item);
            Context.SaveChanges();
        }

        public async Task DeleteItemByIdAsync(int id)
        {
            Context.Set<Comment>().Remove(await GetItemByIdAsync(id));
            await Context.SaveChangesAsync();
        }
    }
}
