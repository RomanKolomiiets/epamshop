﻿
using EpamShop.Domain.Orders;
using EpamShop.Domain.Products;
using EpamShop.Domain.Users;
using System.Collections.Generic;
using System.Data.Entity;

namespace EpamShop.Repository
{
    public class ShopContextInitializer : DropCreateDatabaseAlways<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            var user1 = new User("John","John@gmail.com","0934567898");
            user1.Id = 1;
            var user2 = new User("Bob", "Bob@gmail.com", "0937891265");
            user2.Id = 2;
            var comment1 = new Comment(System.DateTime.Today, "Best apples in the world", user1);
            var comment2 = new Comment(System.DateTime.Today, "Very delicious", user2);
            var product = new Product("Apple", 20, Category.Other, user1, new List<Comment>() {comment1, comment2});
            user1.Products = new List<Product>() { product };
            var order = new Order(new List<Product>() { product });
            user2.Orders = new List<Order>() { order };

            context.Users.Add(user1);
            context.Users.Add(user2);
            context.Products.Add(product);
            context.Orders.Add(order);

            base.Seed(context);
        }
    }
}
