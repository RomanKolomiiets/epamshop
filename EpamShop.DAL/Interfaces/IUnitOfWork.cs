﻿using EpamShop.Domain.Orders;
using EpamShop.Domain.Products;
using EpamShop.Domain.Users;
using System.Threading.Tasks;

namespace EpamShop.Repository.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Comment> CommentRepository { get; }
        IRepository<Order> OrderRepository { get; }
        IRepository<Product> ProductRepository { get; }
        IRepository<User> UserRepository { get; }

        Task<int> SaveAsync();

    }
}
