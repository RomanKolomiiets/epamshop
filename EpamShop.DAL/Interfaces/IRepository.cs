﻿using EpamShop.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EpamShop.Repository.Interfaces
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        Task<T> GetItemByIdAsync(int id);
        Task AddAsync(T item);
        void Update(T item);
        void Delete(T item);
        Task DeleteItemByIdAsync(int id);
    }
}
