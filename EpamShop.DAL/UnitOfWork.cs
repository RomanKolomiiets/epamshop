﻿using EpamShop.Domain.Orders;
using EpamShop.Domain.Products;
using EpamShop.Domain.Users;
using EpamShop.Repository.Interfaces;
using EpamShop.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EpamShop.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ShopContext shopContext = new ShopContext();
        private CommentRepository commentRepository;
        private OrderRepository orderRepository;
        private ProductRepository productRepository;
        private UserRepository userRepository;
        public IRepository<Comment> CommentRepository
        {
            get
            {
                if(commentRepository == null)
                {
                    commentRepository = new CommentRepository(shopContext);
                }
                return commentRepository;
            }
        }

        public IRepository<Order> OrderRepository
        {
            get
            {
                if (orderRepository == null)
                {
                    orderRepository = new OrderRepository(shopContext);
                }
                return orderRepository;
            }
        }

        public IRepository<Product> ProductRepository
        {
            get
            {
                if (productRepository == null)
                {
                    productRepository = new ProductRepository(shopContext);
                }
                return productRepository;
            }
        }

        public IRepository<User> UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(shopContext);
                }
                return userRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await shopContext.SaveChangesAsync();
        }
    }
}
