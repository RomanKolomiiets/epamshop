﻿using EpamShop.Domain.DTO;
using EpamShop.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamShop.UI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderController : ControllerBase
    {
        public IOrderService OrderService { get; set; }

        public OrderController(IOrderService orderService)
        {
            OrderService = orderService;
        }

        // GET: api/<OrderController>
        [HttpGet]
        public ActionResult<IEnumerable<OrderDTO>> Get()
        {
            var products = OrderService.GetAll().ToList();
            if (products == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(products);
            }
        }

        // GET api/<OrderController>/5
        [HttpGet("{id}")]
        public ActionResult<OrderDTO> Get(int id)
        {
            var product = OrderService.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(product);
            }
        }

        // POST api/<OrderController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] OrderDTO product)
        {
            if (product == null)
            {
                return BadRequest();
            }
            else
            {
                await OrderService.AddAsync(product);
                return Ok();
            }
        }

        // PUT api/<OrderController>/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] OrderDTO product)
        {
            if (product == null)
            {
                return BadRequest();
            }
            else
            {
                await OrderService.UpdateAsync(product);
                return Ok();
            }
        }

        // DELETE api/<OrderController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (OrderService.GetByIdAsync(id) != null)
            {
                await OrderService.DeleteByIdAsync(id);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}