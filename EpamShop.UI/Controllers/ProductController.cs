﻿using EpamShop.Domain;
using EpamShop.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EpamShop.UI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        public IProductService ProductService { get; set; }

        public ProductController(IProductService productService)
        {
            ProductService = productService;
        }

        // GET: api/<ProductController>
        [HttpGet]
        public ActionResult<IEnumerable<ProductDTO>> Get()
        {
            var products = ProductService.GetAll().ToList();
            if (products == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(products);
            }
        }

        // GET api/<ProductController>/5
        [HttpGet("{id}")]
        public ActionResult<ProductDTO> Get(int id)
        {
            var product = ProductService.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(product);
            }
        }

        // POST api/<ProductController>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProductDTO product)
        {
            if(product == null)
            {
                return BadRequest();
            }
            else
            {
                await ProductService.AddAsync(product);
                return Ok();
            }
        }

        // PUT api/<ProductController>/5
        [Authorize]
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ProductDTO product)
        {
            if (product == null)
            {
                return BadRequest();
            }
            else
            {
                await ProductService.UpdateAsync(product);
                return Ok();
            }      
        }

        // DELETE api/<ProductController>/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if(ProductService.GetByIdAsync(id) != null)
            {
                await ProductService.DeleteByIdAsync(id);
                return Ok();
            }
            else
            {
                return NotFound();
            }  
        }
    }
}
